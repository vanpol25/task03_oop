package com.polahniuk.service;

import com.polahniuk.ListData;
import com.polahniuk.product.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Class can create an console user interface.
 * Used pattern singleton.
 * Work with data {@link ListData}.
 * @version beta 1
 * @author Ivan Polahniuk
 */
public class ProductService {

    private static ProductService productService;
    private Scanner scanner = new Scanner(System.in);
    private ListData listData = ListData.getData();
    private List<Product> presentList;

    private ProductService() {
    }

    public static ProductService getProductService() {
        if (productService == null) {
            productService = new ProductService();
        }
        return productService;
    }

    public void start() {
        boolean run = true;

        list();
        while (run) {
            switch (scanner.next()) {
                case "1":
                    showList(listData.getAll());
                    break;
                case "2":
                    presentList = new ArrayList<>();
                    listData.getAll().forEach(e -> {
                        if (e instanceof Electronic) {
                            presentList.add(e);
                        }
                    });
                    showList(presentList);
                    findByPrice();
                    break;
                case "3":
                    presentList = new ArrayList<>();
                    listData.getAll().forEach(e -> {
                        if (e instanceof Furniture) {
                            presentList.add(e);
                        }
                    });
                    showList(presentList);
                    findByPrice();
                    break;
                case "4":
                    presentList = new ArrayList<>();
                    listData.getAll().forEach(e -> {
                        if (e instanceof Kitchen) {
                            presentList.add(e);
                        }
                    });
                    showList(presentList);
                    findByPrice();
                    break;
                case "5":
                    presentList = new ArrayList<>();
                    listData.getAll().forEach(e -> {
                        if (e instanceof Plumbing) {
                            presentList.add(e);
                        }
                    });
                    showList(presentList);
                    findByPrice();
                    break;
                case "list":
                    list();
                    break;
                case "6":
                    run = false;
                    break;
                case "7":
                    presentList = listData.getAll();
                    findByPrice();
                    break;
                default:
                    System.err.println("Invalid command!!!");
                    break;
            }
        }
    }

    private void list() {
        System.out.println("Choose option: \n" +
                "1. Show all products\n" +
                "2. Show all electronic\n" +
                "3. Show all furniture\n" +
                "4. Show all kitchen\n" +
                "5. Show all plumbing\n" +
                "6. Exit\n" +
                "7. Find all by price\n");
    }

    private void showList(List<Product> list) {
        list.forEach(System.out::println);
    }

    private void findByPrice() {
        System.out.println("Do you want to find by price? y/n");
        if (scanner.next().equals("y")) {
            int minPrice;
            int maxPrice;
            System.out.print("Find by price:\n" +
                    "min = ");
            minPrice = scanner.nextInt();
            System.out.println("max = ");
            maxPrice = scanner.nextInt();
            showList(listData.getByPrice(presentList, minPrice, maxPrice));
        }
    }
}
