package com.polahniuk;

import com.polahniuk.dto.Brand;
import com.polahniuk.dto.Size;
import com.polahniuk.product.Electronic;
import com.polahniuk.product.Furniture;
import com.polahniuk.product.Kitchen;
import com.polahniuk.product.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Class which I create to use as parody of data base.
 * Used pattern singleton.
 * Used collection List {@link ListData#products}.
 * @version beta 1
 * @author Ivan Polahniuk
 */
public class ListData {

    private List<Product> products = new ArrayList<Product>();

    private static ListData data;

    private ListData() {

    }

    public static ListData getData() {
        if (data == null) {
            data = new ListData();
        }
        return data;
    }

    {
        products.add(new Electronic("Telephone", 200, 31, null,
                new Brand("iPhone", "Plus")));
        products.add(new Electronic("Electric Guitar", 500, 2, null,
                new Brand("GuitarInc", "Type 2")));
        products.add(new Electronic("Headphones", 80, 10, null,
                new Brand("Marshall", "Major")));
        products.add(new Electronic("TV", 450, 16, 80,
                new Brand("Samsung", "K220")));

        products.add(new Furniture("Cupboard", 700, 1, new Size(100, 200, 300)));
        products.add(new Furniture("Chair", 200, 3, new Size(100, 100, 100)));
        products.add(new Furniture("Sofa", 250, 2, new Size(100, 200, 70)));
        products.add(new Furniture("Table", 50, 6, new Size(100, 200, 100)));

        products.add(new Kitchen("Multicooker", 200, 2, new Size(), 600, new Brand("Bravis", "SuperCook")));
        products.add(new Kitchen("Stove", 800, 3, new Size(100, 100, 100), 3000, new Brand("Samsung", "t3000")));
        products.add(new Kitchen("Mixer", 50, 20, new Size(), 100, new Brand("Makita", "Universal")));
        products.add(new Kitchen("Coffee machine", 400, 2, new Size(50, 30, 40), 200, new Brand("CoffeeInc", "Brown")));
    }

    public List<Product> getAll() {
        return products;
    }

    public void addProduct(Product product) {
        products.add(product);
    }

    public void removeProduct(int index) {
        products.remove(index);
    }

    public void removeProduct(Product product) {
        try {
            products.remove(product);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println();
        }
    }

    public List<Product> getByPrice(int min, int max) {
        return findByMinMaxPrice(products, min, max);
    }

    public List<Product> getByPrice(List<Product> productList, int min, int max) {
        return findByMinMaxPrice(productList, min, max);
    }

    private List<Product> findByMinMaxPrice(List<Product> productList, int min, int max) {
        List<Product> newList = new ArrayList<>();
        productList.forEach(e -> {
            if (e.getPrice() >= min && e.getPrice() <= max) {
                newList.add(e);
            }
        });
        return newList;
    }

}
