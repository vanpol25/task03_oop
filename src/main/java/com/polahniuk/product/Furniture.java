package com.polahniuk.product;

import com.polahniuk.dto.Size;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class which extends of {@link Product}
 * Create objects with furniture
 * @version beta 1
 * @author Ivan Polahniuk
 */
@Data
@NoArgsConstructor
public class Furniture extends Product {

   private Size size;

    public Furniture(String name, Integer price, Integer quantity, Size size) {
        super(name, price, quantity);
        this.size = size;
    }

    @Override
    public String toString() {
        return "Furniture{" +
                "name='" + super.getName() + '\'' +
                ", price=" + super.getPrice() +
                ", quantity=" + super.getQuantity() +
                ", size=" + size +
                '}';
    }
}
