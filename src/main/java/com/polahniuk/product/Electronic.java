package com.polahniuk.product;

import com.polahniuk.dto.Brand;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class which extends of {@link Product}
 * Create objects with electronic devices
 * @version beta 1
 * @author Ivan Polahniuk
 */
@Data
@NoArgsConstructor
public class Electronic extends Product {

    private Integer power;
    private Brand brand;

    public Electronic(String name, Integer price, Integer quantity, Integer power, Brand brand) {
        super(name, price, quantity);
        this.power = power;
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "Electronic{" +
                "name='" + super.getName() + '\'' +
                ", price=" + super.getPrice() +
                ", quantity=" + super.getQuantity() +
                ", power=" + power +
                ", brand=" + brand +
                '}';
    }
}
