package com.polahniuk.product;

import com.polahniuk.dto.Brand;
import com.polahniuk.dto.Size;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class which extends of {@link Product}
 * Create objects with kitchen devices and items
 * @version beta 1
 * @author Ivan Polahniuk
 */
@Data
@NoArgsConstructor
public class Kitchen extends Product {

    private Size size;
    private Integer power;
    private Brand brand;

    public Kitchen(String name, Integer price, Integer quantity, Size size, Integer power, Brand brand) {
        super(name, price, quantity);
        this.size = size;
        this.power = power;
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "Kitchen{" +
                "name='" + super.getName() + '\'' +
                ", price=" + super.getPrice() +
                ", quantity=" + super.getQuantity() +
                ", size=" + size +
                ", power=" + power +
                ", brand=" + brand +
                '}';
    }
}