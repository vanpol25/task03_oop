package com.polahniuk.product;

import com.polahniuk.dto.Size;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class which extends of {@link Product}
 * Create objects with items in plumbing.
 * @version beta 1
 * @author Ivan Polahniuk
 */
@Data
@NoArgsConstructor
public class Plumbing extends Product {

    private Size size;

    public Plumbing(String name, Integer price, Integer quantity, Size size) {
        super(name, price, quantity);
        this.size = size;
    }

    @Override
    public String toString() {
        return "Plumbing{" +
                "name='" + super.getName() + '\'' +
                ", price=" + super.getPrice() +
                ", quantity=" + super.getQuantity() +
                ", size=" + size +
                '}';
    }
}
