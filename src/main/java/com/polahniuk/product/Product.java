package com.polahniuk.product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Abstract class with all necessary fields
 * to create enjoyable class.
 * @version beta 1
 * @author Ivan Polahniuk
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class Product {

    private String name;

    private Integer price;

    private Integer quantity;

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
