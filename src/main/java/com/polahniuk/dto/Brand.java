package com.polahniuk.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * You can create an object with brand and model name.
 * @version beta 1
 * @author Ivan Polahniuk
 */
@Data
@AllArgsConstructor
public final class Brand {

    private static final String DEFAULT_BRAND = "Noname";
    private static final String DEFAULT_MODEL = "NoModel";

    private String brand;
    private String model;

    public Brand() {
        model = DEFAULT_MODEL;
        brand = DEFAULT_BRAND;
    }
}
