package com.polahniuk.dto;

import lombok.Data;

/**
 * Class has fields with height, width and length.
 * Can generate volume based on this fields.
 * @version beta 1
 * @author Ivan Polahniuk
 */

@Data
public final class Size {

    private static final Integer DEFAULT_SCALE = 0;

    private Integer height;
    private Integer width;
    private Integer length;
    private Integer volume;

    public Size(Integer height, Integer width, Integer length) {
        this.height = height;
        this.width = width;
        this.length = length;
        this.volume = calcVolume();
    }

    public Size() {
        this.height = DEFAULT_SCALE;
        this.width = DEFAULT_SCALE;
        this.length = DEFAULT_SCALE;
        this.volume = calcVolume();
    }

    private Integer calcVolume() {
        return height * width * length;
    }

}
