package com.polahniuk;

import com.polahniuk.service.ProductService;

public class Application {
    public static void main(String[] args) {
        ProductService ps = ProductService.getProductService();
        ps.start();
    }
}
